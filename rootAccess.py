import user
import sys
import ROOT
import csv
#import PyCintex
import AthenaROOTAccess.transientTree

def loop(begin, end):
    """Convert a pair of C++ iterators into a python generator"""
    while (begin != end):
        yield begin.__deref__()  #*b
        begin.__preinc__()       #++b 

def getDaughters(part):
    decayVtx = part.end_vertex()
    if decayVtx:
        pOut_beg = decayVtx.particles_out_const_begin()
        pOut_end = decayVtx.particles_out_const_end()
        return [x for x in loop(pOut_beg,pOut_end)]
    else:
        return list()
def getParents(part):
    prodVtx = part.production_vertex()
    if prodVtx:
        pIn_beg     = prodVtx.particles_in_const_begin()
        pIn_end     = prodVtx.particles_in_const_end()
        return [x for x in loop(pIn_beg,pIn_end)]
    else:
        return list()

def getSiblings(part):
    prodVtx = part.production_vertex()
    if prodVtx:
        pIn_beg     = prodVtx.particles_out_const_begin()
        pIn_end     = prodVtx.particles_out_const_end()
        return [x for x in loop(pIn_beg,pIn_end)]
    else:
        return list()

def getLastInChain(part):
    if len([x for x in getDaughters(part) if x.pdg_id() == part.pdg_id()])==0 :
        return part
    else:
        for x in getDaughters(part):
            if x.pdg_id() == part.pdg_id():
                getLastInChain(x)
#f = ROOT.TFile.Open("/eos/atlas/user/n/narayan/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.EVNT/EVNT.12458444._006021.pool.root.1")
ch=ROOT.AthenaROOTAccess.TChainROOTAccess('CollectionTree')
with open(sys.argv[1]) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for li in csv_reader:
            for smpl in li:
                ch.AddFile(smpl)

#f = ROOT.TFile.Open(sys.argv[1])

#tt = AthenaROOTAccess.transientTree.makeTree(f)
tt = AthenaROOTAccess.transientTree.makeTree(ch)


fOut    = ROOT.TFile("output.root","RECREATE")
treOut  = ROOT.TTree("selection","photon to lepton selection")

from array import array
import numpy as n


#weight      = array('d',[0])
#phStatus    = array('i',[0])
#phMass      = array('d',[0])
#phPt        = array('d',[0])
#lep1id      = array('i',[0])
#lep1eta     = array('d',[0])
#lep1pt      = array('d',[0])
#lep1phi     = array('d',[0])
#lep2id      = array('i',[0])
#lep2eta     = array('d',[0])
#lep2pt      = array('d',[0])
#lep2phi     = array('d',[0])
#
#treOut.Branch("EventNumber",EventNumber,"EventNumber/I")
#treOut.Branch("weight",weight,"weight/D")
#treOut.Branch("phStatus",phStatus,"phStatus/I")
#treOut.Branch("lep1id",lep1id,"lep1id/I")
#treOut.Branch("lep1eta",lep1eta,"lep1eta/D")
#treOut.Branch("lep1pt",lep1pt,"lep1pt/D")
#treOut.Branch("lep1phi",lep1phi,"lep1phi/D")
#treOut.Branch("lep2id",lep2id,"lep2id/I")
#treOut.Branch("lep2eta",lep2eta,"lep2eta/D")
#treOut.Branch("lep2pt",lep2pt,"lep2pt/D")
#treOut.Branch("lep2phi",lep2phi,"lep2phi/D")
#treOut.Branch("phMass",phMass,"phMass/D")
#treOut.Branch("phPt",phPt,"phPt/D")

EventNumber  = array('L',[0])
weight       = array('d',[0])
parentStatus = array('i',[0])
parentPDG    = array('i',[0])
lepPairMass = array('d',[0])
lepPairPt   = array('d',[0])
lep1id      = array('i',[0])
lep1bc      = array('i',[0])
lep1eta     = array('d',[0])
lep1pt      = array('d',[0])
lep1E       = array('d',[0])
lep1phi     = array('d',[0])
lep2id      = array('i',[0])
lep2bc      = array('i',[0])
lep2eta     = array('d',[0])
lep2pt      = array('d',[0])
lep2E       = array('d',[0])
lep2phi     = array('d',[0])

treOut.Branch("EventNumber",EventNumber,"EventNumber/I")
treOut.Branch("weight",weight,"weight/D")
treOut.Branch("parentStatus",parentStatus,"parentStatus/I")
treOut.Branch("parentPDG",parentPDG,"parentPDG/I")
treOut.Branch("lep1id",lep1id,"lep1id/I")
treOut.Branch("lep1bc",lep1bc,"lep1bc/I")
treOut.Branch("lep1eta",lep1eta,"lep1eta/D")
treOut.Branch("lep1pt",lep1pt,"lep1pt/D")
treOut.Branch("lep1E",lep1E,"lep1E/D")
treOut.Branch("lep1phi",lep1phi,"lep1phi/D")
treOut.Branch("lep2id",lep2id,"lep2id/I")
treOut.Branch("lep2bc",lep2bc,"lep2bc/I")
treOut.Branch("lep2eta",lep2eta,"lep2eta/D")
treOut.Branch("lep2pt",lep2pt,"lep2pt/D")
treOut.Branch("lep2E",lep2E,"lep2E/D")
treOut.Branch("lep2phi",lep2phi,"lep2phi/D")
treOut.Branch("lepPairMass",lepPairMass,"lepPairMass/D")
treOut.Branch("lepPairPt",lepPairPt,"lepPairPt/D")

for iev in tt:
    evt = iev.GEN_EVENT[0]
    EventNumber[0] = iev.McEventInfo.event_ID().event_number()
    runNumber = iev.McEventInfo.event_ID().run_number()
    weight[0]   = evt.weights().front()
    beg = evt.particles_begin()
    end = evt.particles_end()
    selPair = []
    #print "EventNumber: ",EventNumber[0]
    cand_topo = []
    for part in loop(beg,end):
        if (abs(part.pdg_id())==11 or abs(part.pdg_id())==13 and part.status()==1):
            prodVtx     = part.production_vertex()
            if prodVtx:
                pOut_beg    = prodVtx.particles_out_const_begin()
                pIn_beg     = prodVtx.particles_in_const_begin()
                pOut_end    = prodVtx.particles_out_const_end()
                pIn_end     = prodVtx.particles_in_const_end()

                daughterpair=()
                for sibling in loop(pOut_beg,pOut_end):
                    if ((sibling.pdg_id()) == -(part.pdg_id())):
                        daughterpair = (part,sibling)

                if daughterpair:
                    if daughterpair[0] not in sum(cand_topo,()):
                        for parent in loop(pIn_beg,pIn_end):
                            cand_topo.append((parent,daughterpair[0],daughterpair[1]))
    if len(cand_topo)>0:
        for i in xrange(len(cand_topo)):
        #print "Parent: ",cand_topo[0][0].pdg_id()," children: ",cand_topo[0][1].pdg_id()," ", cand_topo[0][2].pdg_id()
            print "Parent: %d(%d) child1: %d(%d) child2: %d(%d)"%(cand_topo[i][0].pdg_id(),cand_topo[i][0].barcode(),cand_topo[i][1].pdg_id(),cand_topo[i][1].barcode(),cand_topo[i][2].pdg_id(),cand_topo[i][2].barcode())
            parentStatus[0] = cand_topo[i][0].status()
            parentPDG[0]   = cand_topo[i][0].pdg_id()

            lep1id[0]      = cand_topo[i][1].pdg_id()
            lep1bc[0]      = cand_topo[i][1].barcode()
            lep1eta[0]     = cand_topo[i][1].momentum().eta()
            lep1pt[0]      = cand_topo[i][1].momentum().perp()
            lep1E[0]       = cand_topo[i][1].momentum().e()
            lep1phi[0]     = cand_topo[i][1].momentum().phi()

            lep2id[0]      = cand_topo[i][2].pdg_id()
            lep2bc[0]      = cand_topo[i][2].barcode()
            lep2eta[0]     = cand_topo[i][2].momentum().eta()
            lep2pt[0]      = cand_topo[i][2].momentum().perp()
            lep2E[0]       = cand_topo[i][2].momentum().e()
            lep2phi[0]     = cand_topo[i][2].momentum().phi()

            lv1         = ROOT.TLorentzVector()
            lv2         = ROOT.TLorentzVector()
            lv1.SetPtEtaPhiE(lep1pt[0],lep1eta[0],lep1phi[0],lep1E[0])
            lv2.SetPtEtaPhiE(lep2pt[0],lep2eta[0],lep2phi[0],lep2E[0])

            lepPairMass[0]  = (lv1 + lv2).M()
            lepPairPt[0]    = (lv1 + lv2).Perp()

            treOut.Fill()

    #for part in loop(beg,end):
    #    if abs(part.pdg_id()) == 6:
    #        decayVtx = part.end_vertex()
    #        li_daughters=[]
    #        if (decayVtx):
    #            pOut_begin = decayVtx.particles_out_const_begin()
    #            pOut_end   = decayVtx.particles_out_const_end()
    #            for daughter in loop(pOut_begin,pOut_end):
    #                li_daughters.append(daughter)
    #                if abs(daughter.pdg_id() ==11):
    #                    print "Electron Found"
    #for part in loop(beg,end):
    #    if abs(part.pdg_id())==11 and part.status() ==1 and len(getSiblings(part))==2 and getSiblings(part)[0].pdg_id() == -getSiblings(part)[1].pdg_id():
    #        selPair.append((getSiblings(part)[0],getSiblings(part)[1]))
    #for x in selPair:
    #    lv1 = ROOT.TLorentzVector()
    #    lv2 = ROOT.TLorentzVector()
    #    lv1.SetPxPyPzE(x[0].momentum().px(),x[0].momentum().py(),x[0].momentum().pz(),x[0].momentum().e())
    #    lv2.SetPxPyPzE(x[1].momentum().px(),x[1].momentum().py(),x[1].momentum().pz(),x[1].momentum().e())

    #    if ( lv1 + lv2 ).M() >1000 :
    #        print [(x[0].pdg_id(),y.pdg_id()) for y in getParents(x[0])],[(x[1].pdg_id(),y.pdg_id()) for y in getParents(x[1])]




            #print [x.pdg_id() for x in getDaughters(part)]
    #    if(abs(part.pdg_id())==11):
    #        sel_parents = []
    #        prodVtx = part.production_vertex()
    #        if prodVtx:
    #            pP_beg  = part.production_vertex().particles_in_const_begin()
    #            pP_end  = part.production_vertex().particles_in_const_end();
    #            for parent in loop(pP_beg,pP_end):
    #                if part.status()==1 and parent.pdg_id() != 111 and abs(parent.pdg_id()) != 24 and abs(parent.pdg_id()) !=15:
    #                    print parent.pdg_id()
    #        else: 
    #             print "No production vertex"

        #if(part.pdg_id() == 22 and part.status() >=50 and part.status()<60):
        #    pOut_beg = part.end_vertex().particles_out_const_begin()
        #    pOut_end = part.end_vertex().particles_out_const_end()
        #    
        #    sel_daughter=[]
        #    for daughter in loop(pOut_beg,pOut_end):
        #        if(abs(daughter.pdg_id())==11 or abs(daughter.pdg_id()==13)):
        #            sel_daughter.append(daughter)

        #    if(len(sel_daughter) ==2):
        #        lvd1 = ROOT.TLorentzVector()
        #        lvd2 = ROOT.TLorentzVector()
        #        lvd1.SetPxPyPzE(sel_daughter[0].momentum().px(),sel_daughter[0].momentum().py(),sel_daughter[0].momentum().pz(),sel_daughter[0].momentum().e())
        #        lvd2.SetPxPyPzE(sel_daughter[1].momentum().px(),sel_daughter[1].momentum().py(),sel_daughter[1].momentum().pz(),sel_daughter[1].momentum().e())
        #        
        #        gamma_lv = (lvd1 + lvd2)
        #        phMass[0]   = gamma_lv.M()
        #        phPt[0]     = gamma_lv.Perp()
        #        phStatus[0] = part.status()

        #        print gamma_lv.M()
        #        print gamma_lv.Perp()
        #        
        #        lep1id[0]   = sel_daughter[0].pdg_id()
        #        lep1eta[0]  = sel_daughter[0].momentum().eta()
        #        lep1pt[0]   = sel_daughter[0].momentum().perp()
        #        lep1phi[0]  = sel_daughter[0].momentum().phi()
        #        lep2id[0]   = sel_daughter[1].pdg_id()
        #        lep2eta[0]  = sel_daughter[1].momentum().eta()
        #        lep2pt[0]   = sel_daughter[1].momentum().perp()
        #        lep2phi[0]  = sel_daughter[1].momentum().phi()
        #        treOut.Fill()
treOut.Write()
fOut.Close()